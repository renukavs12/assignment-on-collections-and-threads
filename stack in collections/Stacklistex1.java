
import java.util.Stack;

public class Stacklistex1 {
	public static void main(String args[]){  
		  Stack<String> set=new Stack<String>();  
		           set.add("Chemistry");  
		           set.add("Mathematics");  
		           set.add("Biology");  
		           set.add("English");  
		           System.out.println("The first set of textbooks contained in first stack are: "+set);  
		           
		  Stack<String> set1=new Stack<String>();  
		           set1.add("Biology");  
		           set1.add("English"); 
		           set1.add("Geography"); 
		           set1.add("Physics"); 
		           System.out.println("The second set of textbooks contained in second stack are: "+set1);  
		
		set.retainAll(set1);  
		System.out.println("Present in both stack: "+ set);   
}
}
