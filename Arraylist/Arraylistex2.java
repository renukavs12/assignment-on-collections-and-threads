import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Course1 {
    private String coursename;
    private String studentname;
    
    
public Course1(String coursename, String studentname) {
		
		this.coursename = coursename;
		this.studentname = studentname;
	}


public Course1(String string, String string2, int i) {
	// TODO Auto-generated constructor stub
}


public String getcoursename() {
	return coursename;
}


public void setcoursename(String coursename) {
	this.coursename = coursename;
}


public String getStudentname() {
	return studentname;
}


public void setStudentname(String studentname) {
	this.studentname = studentname;
}


 @Override
 public String toString() {
     return "{" + 
             "Course name:'" + coursename+ '\'' +
             ",Student name:" + studentname +
             '}';
 }
}

public class Arraylistex2 {
	public static void main(String[] args) {
		List<Course1> student =  new ArrayList<>();
		  student.add(new Course1("Java","John"));
		  student.add(new Course1("Python","David"));
		
		  
		  System.out.println("Student list who attended the courses : " +'\n' +student);
	}

}
