import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Course {
    private String coursename;
    private String studentname;
    private Integer days;
    
    public Course(String coursename, String studentname, Integer days) {
		
		this.coursename = coursename;
		this.studentname = studentname;
		this.days = days;
	}
    
public String getCoursename() {
		return coursename;
	}
public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
public String getStudentname() {
		return studentname;
	}

public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
public Integer getdays() {
		return days;
	}
public void setdays(Integer days) {
		this.days = days;
	}





	@Override
    public String toString() {
        return "{" +
                " Coursename:'" + coursename + '\'' +
                ",StudentName:" + studentname +
                ", days attended:" + days +
                '}';
    }
}

public class ArrayListtex3 {
    public static void main(String[] args) {
        List<Course> student = new ArrayList<>();
        student.add(new Course( "Java","Trev",2));
        student.add(new Course("Java","Chris", 1));
        student.add(new Course("Java","Jill", 10));
        

        System.out.println("student List : " + student);

        // Sort People by their days
        student.sort((student1, student2) -> {
            return student1.getdays() - student2.getdays();
      
        });
        System.out.println("Sorting the days attended the course in ascending order:\n" +student);

       
}
}
